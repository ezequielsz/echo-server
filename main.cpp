#include "srv.hpp"

#define PORT_START	8888
#define PORT_COUNT	5
#define MAX_CLIENTS	50
#define MAX_BACKLOG	10

int main()
{
	server srv(PORT_START, PORT_COUNT, MAX_CLIENTS, MAX_BACKLOG);
	srv.start();
}
