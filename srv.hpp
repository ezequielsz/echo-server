#ifndef SRV_H
#define SRV_HPP

#include <sys/epoll.h>
#include <netinet/in.h>
#include <QThreadPool>
#include <QRunnable>
#include <QMutex>
#include <set>


using namespace std;

class handle_client_data_task : public QRunnable {
public:
	handle_client_data_task(int fd, int buffer_size, QMutex *mutex);
	~handle_client_data_task();
protected:
	void run();
private:
	int fd, buffer_size;
	char *buffer;
	QMutex *mutex;

	void conn_closed();
	void echo(int readed_bytes);
};

class server {
public:
	server(int port_start, int port_count,
	       int max_clients, int max_backlog);
	~server();
	void start();

private:
	const int PORT_START;
	const int PORT_COUNT;
	const int MAX_CLIENTS;
	const int MAX_BACKLOG;

	QThreadPool *thrpool;

	struct epoll_event *events;
	set<int> server_socks;
	int epollfd;

	void set_nonblock_flag(int fd);
	void create_epoll();
	void create_sockets();

	bool is_new_conn(int fd, int *socket);
	void create_new_conn(int fd);
};


#endif // SRV_HPP
