#include "srv.hpp"
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <stdio.h>


server::server(int port_start, int port_count, int max_clients, int max_backlog)
	: PORT_START(port_start), PORT_COUNT(port_count),
	  MAX_CLIENTS(max_clients), MAX_BACKLOG(max_backlog)
{
	thrpool = new QThreadPool();
	thrpool->setMaxThreadCount(PORT_COUNT);

	create_epoll();
	events = new epoll_event[MAX_CLIENTS];
	create_sockets();
}

server::~server()
{
	thrpool->waitForDone(0);
	delete thrpool;

	delete [] events;

	set<int>::iterator it;
	for(it = server_socks.begin(); it != server_socks.end(); ++it)
		close(*it);
}

void server::start()
{
	QMutex mutex;
	
	// Accept the incoming connection
	printf("Waiting for connections ...\n");

	while(1) {
		// wait for event
		int nfds = epoll_wait(epollfd, events, MAX_CLIENTS, -1);
		if (nfds == -1) {
			perror("epoll_pwait");
			exit(EXIT_FAILURE);
		}

		for (int n = 0; n < nfds; n++) {
			int fd = events[n].data.fd;
			int socket;

			if (is_new_conn(fd, &socket)) {
				try {
					create_new_conn(socket);
				} catch (int err) {
					printf("Unable to create new connection\n");
				}
			} else {
				handle_client_data_task *task;
				task = new handle_client_data_task(fd, 1025,
					&mutex);
				task->setAutoDelete(true);
				thrpool->start(task);
			}
		}
	}
}

void server::set_nonblock_flag(int fd)
{
	int status = fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
	if (status == -1)
		perror("calling fcntl");
}

void server::create_epoll()
{
	epollfd = epoll_create1(0);
	if (epollfd == -1) {
		perror("epoll_create1");
		exit(EXIT_FAILURE);
	}
}

void server::create_sockets()
{
	struct sockaddr_in address;
	struct epoll_event ev;

	int server_sock = 0;

	// Create a master socket
	for (int i = 0; i < PORT_COUNT; i++) {
		if ((server_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
			perror("socket failed");
			exit(EXIT_FAILURE);
		}

		bzero(&address, sizeof(address));

		// Type of socket created
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = INADDR_ANY;
		address.sin_port = htons(PORT_START + i);

		// Bind the socket to localhost port 8888 + i
		if (bind(server_sock, (struct sockaddr *)&address, sizeof(address)) < 0) {
			perror("bind failed");
			exit(EXIT_FAILURE);
		}

		// Specify maximum of pending connections
		// for the server socket
		if (listen(server_sock, MAX_BACKLOG) < 0) {
			perror("listen");
			exit(EXIT_FAILURE);
		}

		ev.events = EPOLLIN;
		ev.data.fd = server_sock;
		if ((epoll_ctl(epollfd, EPOLL_CTL_ADD, server_sock, &ev)) == -1) {
			perror("epoll_ctl: server_socks");
			exit(EXIT_FAILURE);
		}
		server_socks.insert(server_sock);
	}
}

bool server::is_new_conn(int fd, int *socket)
{
	set<int>::iterator it = server_socks.find(fd);
	if ( it != server_socks.end()) {
		*socket = *it;
		return true;
	}
	return false;
}

void server::create_new_conn(int fd)
{
	struct sockaddr_in addr;
	int addrlen = sizeof(addr);
	struct epoll_event ev;
	int conn_sock = accept(fd,
			       (struct sockaddr *)&addr,
			       (socklen_t*)&addrlen);
	if (conn_sock == -1) {
		perror("accept");
		exit(EXIT_FAILURE);
	}

	set_nonblock_flag(conn_sock);

	// Inform user of socket number,
	// used in send and receive commands
	printf("New connection: socket fd=%d, ip=%s, port=%d (%d)\n",
	       conn_sock, inet_ntoa(addr.sin_addr),
	       ntohs(addr.sin_port), PORT_START);

	ev.events = EPOLLIN | EPOLLET;
	ev.data.fd = conn_sock;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD,
		      conn_sock, &ev) == -1) {
		perror("epoll_ctl: conn_sock");
		exit(EXIT_FAILURE);
	}
}



handle_client_data_task::handle_client_data_task(int fd, int buffer_size,
	QMutex *mutex)
{
	this->fd = fd;
	this->buffer_size = buffer_size;
	buffer = new char[this->buffer_size];
	this->mutex = mutex;
}

handle_client_data_task::~handle_client_data_task()
{
	delete buffer;
}

void handle_client_data_task::run()
{
	// Read the incomming message
	mutex->lock();
	int valread = recv(fd, buffer, (this->buffer_size) - 1, MSG_DONTWAIT);
	mutex->unlock();

	switch (valread) {
	case -1:
		perror("read");
		break;
	case 0:
		conn_closed();
		break;
	default:
		echo(valread);
		break;
	}
}

void handle_client_data_task::echo(int readed_bytes)
{
	// Set the string terminating NULL byte
	// on the end of the data read
	buffer[readed_bytes] = '\0';
	if (send(fd, buffer, strlen(buffer), MSG_DONTWAIT)
			!= (int)strlen(buffer)) {
		perror("send");
	}
}


void handle_client_data_task::conn_closed()
{
	struct sockaddr_in address;
	int socklen = sizeof(address);

	// Somebody disconnected,
	// get his details and print
	getpeername(fd, (struct sockaddr *) &address, (socklen_t *) &socklen);

	printf("Host disconnected, ip=%s, port=%d\n",
	       inet_ntoa(address.sin_addr),
	       ntohs(address.sin_port));

	// Close the socket and mark as 0 in
	// list for reuse
	close(fd);
}
